package question2;

public class Recursion {

	public static void main(String[] args) {
        int[] numbers = new int[10];
        numbers[0] = 21;
        numbers[1] = 52;
        numbers[2] = 34;
        numbers[3] = 24;
        numbers[4] = 12;
        numbers[5] = 11;
        numbers[6] = 50;
        System.out.println(recursiveCount(numbers,2));
	}
	public static int recursiveCount(int[] numbers, int n){
      if (n < numbers.length) {
         //in "Are located within numbers at an odd index (e.g. 1,3,5,7, etc) that is greater than or equal to n."
         //I didn't understand well if we had to compare to the number or the index value
         //But I think the index makes more sense, and since I set n to be the starting index, the index will always be
       	 //greater or equals to n. Just not sure
         if(numbers[n] > 20 && (n % 2 != 0)){
              return recursiveCount(numbers, n + 1) + 1;
           }
         else {
              return recursiveCount(numbers, n + 1);
            }
        }
         return 0;
    }

}
