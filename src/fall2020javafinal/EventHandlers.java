package fall2020javafinal;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import java.util.Random;


public class EventHandlers implements EventHandler<ActionEvent> {
	private TextField betAmount;
	private String betType;
	private TextField result;
	private User c;
	public EventHandlers(TextField tf1, String betType,TextField result, User c) {
		this.betAmount = tf1;
		this.betType = betType;
		this.result = result;
		this.c = c;
	}
	
	@Override
	public void handle(ActionEvent Event) {
		int bet = Integer.parseInt(betAmount.getText());
		if(c.getMoney() >= bet) {
      Random r = new Random();
      int diceRoll = r.nextInt(6) + 1;
      if(betType.equals("small") && diceRoll < 4) {
    	  c.setMoney(c.getMoney() + bet);
    	  result.setText("The dice fell on "+diceRoll+", you have won "+betAmount.getText()+" Total: "+c.getMoney());
      }
      else if(betType.equals("big") && diceRoll > 3){
    	  c.setMoney(c.getMoney() + bet);
    	  result.setText("The dice fell on "+diceRoll+", you have won "+betAmount.getText()+" Total: "+c.getMoney());
      }
      else {
    	  c.setMoney(c.getMoney() - bet);
    	  result.setText("The dice fell on "+diceRoll+", you have lost "+betAmount.getText()+" Total: "+c.getMoney());
      }
	}
    else {
	    	  result.setText("You do not have enough funds");
   }
  }
}
