package fall2020javafinal;

public class User {
	private int money;
   public User(int money) {
	   this.money = money;
   }
   
   public void setMoney(int money) {
	   this.money = money;
   }
   public int getMoney() {
	   return this.money;
   }
}
