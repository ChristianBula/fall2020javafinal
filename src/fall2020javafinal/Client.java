package fall2020javafinal;

    import javafx.application.*;
	import javafx.stage.*;
	import javafx.scene.*;
	import javafx.scene.paint.*;
	import javafx.scene.control.*;
	import javafx.scene.layout.*;

	public class Client extends Application {
		
		private User u = new User(500);
		private EventHandlers e;
		
		public static void main(String[] args) {
			Application.launch(args);
		}
		
		public void start(Stage stage) {
			Group root = new Group();
			Scene scene = new Scene(root, 650, 400);
			scene.setFill(Color.BLACK);
			
		    GridPane grid = new GridPane();
		    
			HBox h1 = new HBox();
			HBox h2 = new HBox();
			HBox h3 = new HBox();
			TextField tf1 = new TextField("How much would you like to bet?");
			tf1.setMinWidth(200);
			
			Button b1 = new Button("Small bet");
			Button b2 = new Button("Big bet");
			TextField tf2 = new TextField("Result");
			e = new EventHandlers(tf1,"small",tf2,u);
			b1.setOnAction(e);
			e = new EventHandlers(tf1,"big",tf2,u);
			b2.setOnAction(e);
			tf2.setMinWidth(270);
			h1.getChildren().add(tf1);
			h2.getChildren().addAll(b1,b2);
			h3.getChildren().add(tf2);
			
			grid.add(h1,0,0);
			grid.add(h2, 0, 1);
			grid.add(h3, 0, 2);

			root.getChildren().add(grid);
			
			
			stage.setTitle("Dice Game");
			stage.setScene(scene);
			stage.show();
			};
		}